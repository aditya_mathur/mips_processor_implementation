//this reg file supports 2 read ports and one write port
module reg_file (
	input CLK,rst,
	input [4:0] read_addr1,
	input [4:0] read_addr2,
	input write_en,
	input [4:0] write_addr,
	input [31:0] write_data,
	output reg [31:0] read_data1,
	output reg [31:0] read_data2
);

integer i;
reg [31:0] int_reg [31:0];//32 internal reg of 32 bit

always @(posedge CLK or negedge !rst) begin
	if(!rst) begin
		int_reg [0] = 32'h01;
		int_reg [1] = 32'h02;
		int_reg [2] = 32'h03;
		int_reg [3] = 32'h04;
		int_reg [4] = 32'h05;
		int_reg [5] = 32'h06;
		int_reg [6] = 32'h07;
		int_reg [7] = 32'h08;
		int_reg [8] = 32'h09;
		int_reg [9] = 32'h0a;
		for(i=10; i<32; i++) begin
			int_reg[i] =32'h00;
		end
	end
	else begin
		if(write_en) begin
			int_reg [write_addr] = write_data;
		end
		else begin
			read_data1 = int_reg[read_addr1];
			read_data2 = int_reg[read_addr2];
		end
	end
end
	
endmodule