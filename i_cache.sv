module i_cache (
	input CLK, rst,
	input [31:0] PC,//program counter
	output reg [31:0] i_data
);

parameter InstNum = 32;
//defining memory to hold 10 instructions of 32 bits
reg [31:0] i_mem [InstNum-1:0];
integer i;

always @(posedge CLK or negedge !rst) begin
	if (!rst) begin
		i_data[0]=32'b00000010010100111000100000100000;//add $r1,$r2,$r3
		i_data[1]=32'b10001110010100010000000000010100; //lw $r1,20($r2) 
		i_data[2]=32'b10101110010100010000000000010100; //sw $r1,20($r2)
		i_data[3]=32'b00000010010100111000100000100111; //nor $r2,r2,r3
		i_data[4]=32'b00010010001100100000000000000101; //beq $r1,$r2,5
		for(i=5;i<=InstNum-1;i++) begin
			i_mem[i]<=32'b0;
		end
	end
	else begin
		if (PC > 0 && PC <  InstNum-1 ) begin
			i_data <= i_mem[PC];
		end
		else begin
			$display("\nInvalid Inst address, reset the core\n\t PC <= 32'b0");
			$display("\n##illegal instruction##\n");
		end
	end

end

endmodule