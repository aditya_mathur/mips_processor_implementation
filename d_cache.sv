//this is data cache for MIP processor implementation
module d_cache (
	input [31:0] read_addr,
	input [31:0] write_addr,
	input [31:0] write_data,
	input CLK, rst,
	input write_en,//this will enable write in d-cache
	output reg [31:0] read_data
);

parameter DCacheSize = 32;

//this is a 32x32bit memory
reg [31:0] d_mem [DCacheSize-1:0];
integer i;

always @(posedge CLK or negedge !rst) begin
	if(!rst) begin
		for(i=0;i<=DCacheSize-1;i++) begin
			d_mem[i] = i;
		end
	end
	else begin
		if (write_en) begin
			d_mem[write_addr[7:2]] <= write_data;
		end
		else begin
			read_data <= d_mem[read_addr[7:2]];
		end
	end
end

	
endmodule