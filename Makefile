COMPILE =  compile 
CLEAN = clean 

compile:
	@echo "compiling ......"
	iverilog -o test d_cache.sv
	@echo "simulation ......."
	vvp test

clean:
	@echo "cleaning ......."
	rm -r test
